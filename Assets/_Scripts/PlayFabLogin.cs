﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;

// On PlayFabLogin Game Object

public class PlayFabLogin : MonoBehaviour
{
    public void Start()
    {
        //Примечание. Установка идентификатора заголовка здесь может быть пропущена, если вы уже установили значение в Расширения редактора.
        if (string.IsNullOrEmpty(PlayFabSettings.TitleId))
        {
            PlayFabSettings.TitleId = "AB76"; // заменяем это значение на собственное 
        }
        var request = new LoginWithCustomIDRequest { CustomId = "GettingStartedGuide", CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginSuccess(LoginResult result)
    {
        Debug.Log("Congratulations, you made your first successful API call!");
    }

    private void OnLoginFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call.  :(");
        Debug.LogError("Here's some debug information:");
        Debug.LogError(error.GenerateErrorReport());
    }
}