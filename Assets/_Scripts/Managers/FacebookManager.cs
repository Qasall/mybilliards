﻿using Facebook.Unity;
using System.Collections.Generic;
using UnityEngine;
using System;

// Автотомный класс
// Регистрация игрока через аккаунт Facebook
// Хранит в себе всё с аккаунта Facebook

[Serializable]
public class FacebookManager
{
    public string MyID;             // хранит ID игрока

    List<string> MyFriendNameList = new List<string>();             // список имён друзей
    List<string> MyFriendIDList = new List<string>();               // список ID друзей
    Dictionary<string, string> MyFriendDictionary = new Dictionary<string, string>();               // словарь друзей по никам


    public FacebookManager()                // Проверка на факт подключения
    {
        if (!FB.IsInitialized)
        {           
            FB.Init(InitCallback, OnHideUnity);              // Инициализировать SDK для Facebook
        }
        else
        {            
            FB.ActivateApp();               // Уже инициализировано сигнальное приложение App Event
        }
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {            
            FB.ActivateApp();               // Сигнал приложения App Event
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {            
            Time.timeScale = 0;             // Приостановите игру - нам нужно скрыть
        }
        else
        {           
            Time.timeScale = 1;              // Возобновите игру - снова получаем фокус
        }
    }

    public void LoginWithFacebook()
    {
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    public void FacebookLogout()
    {
        FB.LogOut();
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            MyID = AccessToken.CurrentAccessToken.UserId;
            FB.API("/me?fields=first_name", HttpMethod.GET, DisplayUsername);
            FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
            FB.API("/me/friends", HttpMethod.GET, GetFriendsPlayingThisGame);
            Managers.Instance.playfabManager.CreatePlayerOnLoginWithFacebook();
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    private void DisplayUsername(IResult result)
    {
        if (result.Error == null)
        {
            Managers.Instance.player.Name = result.ResultDictionary["first_name"].ToString();
        }
        else
        {
            Managers.Instance.player.Name = StaticStrings.ErrorUserName;
            Debug.Log(result.Error);
        }
    }

    void DisplayProfilePic(IGraphResult result)
    {
        if (result.Texture != null)
        {
            Managers.Instance.player.Avatar = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
        }
    }

    public void GetFriendsPlayingThisGame(IResult result)
    {
        if (result.Error == null)
        {
            var dictionary = (Dictionary<string, object>)Facebook.MiniJSON.Json.Deserialize(result.RawResult);

            var friendsList = (List<object>)dictionary["data"];
            foreach (var dict in friendsList)
            {
                MyFriendNameList.Add(((Dictionary<string, object>)dict)["name"].ToString());
                MyFriendIDList.Add(((Dictionary<string, object>)dict)["id"].ToString());
                MyFriendDictionary.Add(((Dictionary<string, object>)dict)["id"].ToString(), ((Dictionary<string, object>)dict)["name"].ToString());
            }
        }
    }
}
