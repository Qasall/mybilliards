﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// класс-синглтон на Manager
// хранит в себе и инициализирует в себе другие менаджеры, 
// перемещается по сценам

public class Managers : MonoBehaviour
{
    public static Managers Instance { get; private set; }           // статический член синглтона

    public FacebookManager facebookManager;
    public PlayfabManager playfabManager;
    public Player player;
    public AdsManager adsManager;

    public List<PlayFab.ClientModels.PlayerLeaderboardEntry> Leaderboard;

    private void Awake()            
    {
        // проверяем на наличие объекта класса
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);          // запрет на уничтожение объекта на новых уровнях
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        // создаём объеты мэнеджеров
        facebookManager = new FacebookManager();
        playfabManager = new PlayfabManager();
        player = new Player();        
        adsManager = new AdsManager();
    }

    public void LoginWithFacebook()             // логинимся от Facebook
    {
        facebookManager.LoginWithFacebook();
    }

    public void SetAvatar(Sprite sprite)            // получаем аватар с Facebook
    {
        player.Avatar = sprite;
    }

    public void AddGold(int amount)         // присваиваем стартовое кол-во Gold
    {
        playfabManager.AddGoldOnDB(amount);
    }

    public void SubstractGold(int amount)           // списывание Gold
    {
        playfabManager.SubtractGoldOnDB(amount);
    }

    public void UpdateLeaderboard(int amount)           // статистика по игроку
    {
        playfabManager.UpdateUserStatistic(amount);
    }

    public void GetLeaderboard()            // вызов лидерборда
    {
        playfabManager.GetLeaderboard();
    }

    public void ShowLeaderboard()           // корректируем показ лидерборда по игроку
    {
        foreach (var item in Leaderboard)
        {
            Debug.Log(item.Position + ": " + item.DisplayName);
        }
    }

    public void ShowInternalAds()
    {
        adsManager.ShowADS();
    }

    public void ShowRewarAds()
    {
        adsManager.ShowRewardedVideo();
    }

}
