﻿using UnityEngine;
using UnityEngine.Advertisements;
using PlayFab;
using System;

[Serializable]
public class AdsManager
{
#if UNITY_IOS   
    private const string IOSGameId = "2844704";
#elif UNITY_ANDROID   
    private const string AndroidGameId = "2844705";
#endif   
    public static bool IsTest = true;

    public AdsManager()
    {
#if UNITY_IOS
        Advertisement.Initialize(IOSGameId, IsTest);
#elif UNITY_ANDROID
        Debug.Log(GetHashCode());
        Advertisement.Initialize(AndroidGameId, IsTest);
#endif
    }

    public void ShowADS()
    {
        if (Advertisement.IsReady())
            Advertisement.Show();
    }

    public void ShowRewardedVideo()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResultCoins;
        Advertisement.Show("rewardedVideo", options);
    }

    private void HandleShowResultCoins(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Debug.Log("Video completed - Offer a reward to the player");
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");
        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }


}

